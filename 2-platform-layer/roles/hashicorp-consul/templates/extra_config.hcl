datacenter  = "{{ dc_name }}"
server      = true

# Secret for Gossip protocol symmetric encryption, Rotate this
encrypt     = "{{ gossip_encrypt_secret | default('FTSOpGadzLWf62zKHvEfkZPXLb3afoh1qs/yO6++tvw=') }}"

bind_addr   = "{{ ansible_host }}"
client_addr = "0.0.0.0"

bootstrap_expect       = 3
verify_server_hostname = true
verify_incoming        = false
verify_outgoing        = true

ca_file                = "/etc/pki/ca-trust/source/anchors/ca.crt"
cert_file              = "/etc/consul.d/consul_{{inventory_hostname}}.crt"
key_file               = "/etc/consul.d/consul_{{inventory_hostname}}.key"

retry_join = {{ hostvars | json_query('*.ansible_host') | to_json }}

ports {
  http  = -1
  https = 8501
}

ui_config {
  enabled = true
}
