data_dir = "/var/lib/nomad"

datacenter = "{{ dc_name }}"

# region = "global"

server {
  enabled          = true
  bootstrap_expect = 3
  encrypt          = "{{ gossip_encrypt_secret | default('pjA+O5UbIJbQ5czZjXRYJPghfFEfYITDir8Tl6b09MQ=') }}"
}

client {
  enabled = true
}

consul {
  address   = "{{ansible_host}}:8501"
  ssl       = true
  ca_file   = "/etc/pki/ca-trust/source/anchors/ca.crt"
  cert_file = "/etc/nomad.d/nomad_{{inventory_hostname}}.crt"
  key_file  = "/etc/nomad.d/nomad_{{inventory_hostname}}.key"
}

tls {
  http                   = true
  rpc                    = true
  ca_file                = "/etc/pki/ca-trust/source/anchors/ca.crt"
  cert_file              = "/etc/nomad.d/nomad_{{inventory_hostname}}.crt"
  key_file               = "/etc/nomad.d/nomad_{{inventory_hostname}}.key"
  verify_server_hostname = true
  verify_https_client    = false
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "nomad-driver-podman" {
  config {
    volumes {
      enabled      = true
      selinuxlabel = "z"
    }
  }
}
