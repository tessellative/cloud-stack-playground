ui = true

listener "tcp" {
  address         = "0.0.0.0:8200"
  cluster_address = "0.0.0.0:8201"
  tls_cert_file   = "/etc/vault.d/vault_node01.crt"
  tls_key_file    = "/etc/vault.d/vault_node01.key"
}

storage "consul" {
  address = "https://{{ ansible_host }}:8501"
  path    = "vault/"
}

