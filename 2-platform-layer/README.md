# Platform Layer

## Overview

- Consul
    - Cloud internal DNS and service registry / discovery
- Nomad
- easyRSA as master / external CA
- Hashicorp Vault (PKI, intermediate CA / Secret store )
    - Server Certs for Ingress (Traefik)

## Prerequisites for local / bastion service resolution

Make sure that the following line exists in your local / bastion /etc/hosts file

    10.10.10.101	haproxy.service.consul traefik.service.consul vault.service.consul consul.service.consul nomad.service.consul prometheus.service.consul grafana.service.consul

# Platform Setup Steps

## Setup EasyRSA and the PKI

- Initialize PKI
- Generate server certs
- Trust CA
- Install server certs

```shell
ansible-playbook 0.0-setup-pki-playbook.yml 0.1-generate-server-certs-playbook.yml 0.2-trust-ca-cert-playbook.yml 0.3-install-server-certs-playbook.yml 
```

At this point the CA cert can be tursted locally to avoid browser and tool warnings on self signed certs

```
./certs/easy-rsa-master/easyrsa3/pki/ca.crt
```

## Platform Base Services

Install and Setup Podman and dnsmasq

```shell
ansible-playbook 1.0-setup-base-platform-playbook.yml 
```

## Setup Consul

```shell
ansible-playbook 1.1-setup-consul-playbook.yml
```

Consul UI dashboard should be available:
https://consul.service.consul:8501/ui/

## Setup Nomad

```shell
ansible-playbook 1.2-setup-nomad-playbook.yml
```

Nomad UI Dashboards should be available:
https://nomad.service.consul:4646/ui/

## Setup Vault

```shell
ansible-playbook 1.3-setup-vault-playbook.yml
```

Vault UI should be available:
https://vault.service.consul:8200/ui/

The login token can be found in: vault_init.secret

