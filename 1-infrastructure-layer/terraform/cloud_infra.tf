resource "libvirt_network" "cloud_network" {
  name      = "cloudnet"
  mode      = "nat"
  autostart = true
  domain    = "cloud.local"
  addresses = ["10.10.10.0/24"]
  dns {
    enabled = true
  }
}

resource "libvirt_pool" "cloud-pool" {
  name = "cloud-pool"
  type = "dir"
  path = "/opt/cloud_pool/storage"
}

resource "libvirt_volume" "vm_base_image" {
  name   = "debian-base-image"
  pool   = libvirt_pool.cloud-pool.name
  source = "https://dl.rockylinux.org/pub/rocky/9/images/x86_64/Rocky-9-GenericCloud.latest.x86_64.qcow2"
}


data "template_file" "user_data" {
  template = file("${path.module}/templates/cloud_init.yml")
  vars     = {
    ssh_pub_key = "${file("../../ssh/id_rsa.pub")}"
  }
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name      = "commoninit.iso"
  pool      = libvirt_pool.cloud-pool.name
  user_data = data.template_file.user_data.rendered
}

variable "node_map" {
  type    = map
  default = {
    "node01" : "10.10.10.101",
    "node02" : "10.10.10.102",
    "node03" : "10.10.10.103"
  }
}

resource "libvirt_volume" "node_volume" {
  for_each       = var.node_map
  name           = each.key
  pool           = libvirt_pool.cloud-pool.name
  size           = 11811160064
  base_volume_id = libvirt_volume.vm_base_image.id
}

resource "libvirt_domain" "cloud_node" {
  for_each  = var.node_map
  name      = each.key
  memory    = "4096"
  vcpu      = 2
  cloudinit = libvirt_cloudinit_disk.commoninit.id

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_name   = libvirt_network.cloud_network.name
    addresses      = [each.value]
    wait_for_lease = true
  }

  disk {
    volume_id = libvirt_volume.node_volume[each.key].id
  }

  console {
    type        = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

locals {
  ips = flatten(values(libvirt_domain.cloud_node)[*].network_interface[0].addresses)
  names = keys(libvirt_domain.cloud_node)
  inventory = templatefile("${path.module}/templates/host.tpl", {
    nodes = zipmap(local.names, local.ips )
  } )
}

# generate inventory file for Ansible
resource "local_file" "inventory" {
  content = local.inventory
  filename = "${path.module}/../../hosts"
  depends_on = [libvirt_domain.cloud_node]
}
