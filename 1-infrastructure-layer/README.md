# Bootstrap the VMs with Terraform

```shell
terraform  -chdir=terraform init
terraform  -chdir=terraform apply
```

Cloud image downloads for the "vm_base_image" terraform resource could take up to 5-6 minutes. 

Make sure that the ../hosts file is properly populated with the VMs' IP addresses

```shell
ansible-playbook 1-setup-playbook.yml
```

The VMs should be up and ready for the platform setup!

## Connection Test

```shell
ssh -i ../ssh/id_rsa manage@10.10.10.101 /bin/sudo /bin/whoami
```
