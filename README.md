# Stack Overview

- KVM / qemu VMs via Terraform
- Linux qcow cloud base image booted with cloud-init 
- Cloud orchestration / Hashicorp stack
- Service discovery
- Metrics and Monitoring

# Tools and Pre-requisites

- terraform / tofu
- vault
- nomad
- consul
- levant
- ansible
- libvirtd
- kvm / qemu

## Generate the initial SSH keys

```shell
ssh-keygen -t rsa -b 4096 -C "manage@cloud.playground"  -f ./ssh/id_rsa -q -N ""
```

## Ansible Setup

```shell
python -m venv ./.venv
source ./.venv/bin/activate
pip install ansible jmespath
```

# 1 - Infrastructure Layer

VM and network setup

3 node cluster

[1-infrastructure-layer](1-infrastructure-layer/README.md)

# 2 Platform Layer

- PKI and certs via EasyRSA
- Hashicorp Stack Setup: Consul, Nomad, Vault

[2-platform-layer](2-platform-layer/README.md)

# 3 Application Deployment Layer

Applications that can be deployed on top of the platform via Hashicorp Levant
- HTTP Ingress for the cluster services
- Monitoring services

[3-application-layer](3-application-layer/README.md)
