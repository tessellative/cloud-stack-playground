# Application Deployment Layer

## Prerequisites

Make sure that the following line exists in your local / bastion /etc/hosts file

    10.10.10.101	haproxy.service.consul traefik.service.consul vault.service.consul consul.service.consul nomad.service.consul prometheus.service.consul grafana.service.consul


### Trust easyRSA Root CA

Also make sure that you trust the easyRSA root CA on your local / bastion machine. Otherwise the certificate generaton and the vault pki endpoint access in this layer will not work!

#### Debian

```shell
sudo cp ./2-platform-layer/certs/easy-rsa-master/easyrsa3/pki/ca.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates
```

#### RHEL / CentOS / etc

```shell
sudo cp ./2-platform-layer/certs/easy-rsa-master/easyrsa3/pki/ca.crt /etc/pki/ca-trust/source/anchors/
sudo update-ca-trust
```

#### Arch 

```shell
sudo trust anchor --store ./2-platform-layer/certs/easy-rsa-master/easyrsa3/pki/ca.crt
```

## HTTP Ingress Cert Initialization

```shell
ansible-playbook 1.0-generate-service-ingress-certificates-playbook.yml
ansible-playbook 1.1-install-service-ingress-certificates-playbook.yml
```

# Deployment of Services via Levant

https://github.com/hashicorp/levant

- HTTP Ingress via HAProxy
- Prometheus
- Grafana

Execute these from your local / bastion host

```shell
export NOMAD_ADDR=https://nomad.service.consul:4646
levant deploy -var-file=./deployment/defaults.yml ./deployment/haproxy-ingress/deploy.hcl
levant deploy -var-file=./deployment/defaults.yml ./deployment/monitoring/deploy.hcl
```

## Nomad Services
    https://nomad.service.consul:4646/ui/jobs

## Haproxy test URL

    http://haproxy.service.consul:1936/

## Grafana 

    https://grafana.service.consul

## Prometheus

    https://prometheus.service.consul
  
