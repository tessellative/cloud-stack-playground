job "traefik" {
  datacenters = ["[[ .datacenter ]]"]
  type        = "system"

  group "traefik_grp" {

    count = 1

    update {
      max_parallel     = 1
      min_healthy_time = "30s"
      auto_revert      = true
    }

    network {
      port "http" {
        static = "80"
      }
      port "https" {
        static = "443"
      }
      port "admin" {
        static = "9002"
      }
    }

    task "copy-ca-certs" {
      driver = "exec"
      config {
        command = "/bin/bash"
        args    = ["-c", "cp /etc/ssl/certs/ca-bundle.crt ${NOMAD_ALLOC_DIR}/data/"]
      }

      lifecycle {
        hook = "prestart"
        sidecar = "false"
      }
    }

    task "traefik" {
      driver = "podman"

      dispatch_payload {
        file = "config.json"
      }

      service {
        name = "traefik-secure"
        port = "https"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
      service {
        name = "traefik"
        port = "admin"
        tags = [
#          "traefik.enable=true",
#          "traefik.http.middlewares.httpsRedirect.redirectscheme.scheme=https",
#          "traefik.http.middlewares.httpsRedirect.redirectscheme.permanent=true",
#          "traefik.http.routers.api.service=api@internal",
#          "traefik.http.routers.${NOMAD_TASK_NAME}_insecure.middlewares=httpsRedirect",
#          "traefik.http.routers.${NOMAD_TASK_NAME}.tls.domains[0].main=${NOMAD_TASK_NAME}.[[ .tld ]]"
        ]

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }

      config {
        image        = "docker.io/traefik:v2.7"
        volumes      = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
          "/etc/traefik/dynamic_certs.toml:/etc/traefik/dynamic_certs.toml",
          "/etc/ssl/service-certs/:/etc/ssl/certs/",
          "/etc/consul.d/:/etc/consul.d/"
        ]
        network_mode = "host"
      }

      template {
        destination     = "local/traefik.toml"
        env             = false
        change_mode     = "noop"
        left_delimiter  = "{{{"
        right_delimiter = "}}}"
        data            = <<EOH
[global]
  checkNewVersion = false
  sendAnonymousUsage = false

[entryPoints]
  [entryPoints.web]
    address = "0.0.0.0:80"
  [entryPoints.traefik]
    address = "0.0.0.0:9002"
  [entryPoints.websecure]
    address = "0.0.0.0:443"

[log]

[api]
  dashboard = true
  insecure = true

[ping]

[metrics]
  [metrics.prometheus]

[providers.consulcatalog]
  exposedByDefault = false
  prefix = "traefik"
  defaultRule = "Host(`{{ .Name }}.[[ .tld ]]`)"
  [providers.consulcatalog.endpoint]
    address = "{{{env "attr.unique.network.ip-address"}}}:8501"
    scheme = "https"
    datacenter = "[[ .datacenter ]]"
    endpointWaitTime = "15s"
    [providers.consulCatalog.endpoint.tls]
      ca = "{{{ env "NOMAD_ALLOC_DIR" }}}/data/ca-bundle.crt"
      cert = "/etc/consul.d/consul_{{{ env "node.unique.name" }}}.crt"
      key = "/etc/consul.d/consul_{{{ env "node.unique.name" }}}.key"

[providers]
  [providers.file]
    filename = "/etc/traefik/dynamic_certs.toml"
    watch    = true

EOH
      }
    }
  }
}
