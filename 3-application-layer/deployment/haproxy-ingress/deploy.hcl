job "haproxy" {
  datacenters = ["[[ .datacenter ]]"]
  type        = "system"

  group "haproxy" {
    count = 1

    update {
      max_parallel     = 1
      min_healthy_time = "30s"
      auto_revert      = true
    }

    network {
      port "http" {
        static = "8080"
      }
      port "https" {
        static = "443"
      }
      port "gui" {
        static = 1936
      }
    }

    task "copy-ca-certs" {
      driver = "exec"
      config {
        command = "/bin/bash"
        args    = ["-c", "cp /etc/ssl/certs/ca-bundle.crt ${NOMAD_ALLOC_DIR}/data/"]
      }

      lifecycle {
        hook = "prestart"
        sidecar = "false"
      }
    }

    service {
      name = "haproxy"
      port = "http"
      check {
        name     = "alive"
        type     = "tcp"
        port     = "http"
        interval = "10s"
        timeout  = "2s"
      }
    }

    service {
      name = "haproxy-secure"
      port = "http"
      check {
        name     = "alive"
        type     = "tcp"
        port     = "https"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "haproxy" {
      driver = "podman"
      user = "root"
      dispatch_payload {
        file = "config.json"
      }

      config {
        image        = "docker.io/bitnami/haproxy:2.6.6"
        volumes      = [
          "local/haproxy.cfg:/bitnami/haproxy/conf/haproxy.cfg",
          "/etc/ssl/service-certs/:/etc/ssl/certs/",
          "/etc/consul.d/:/etc/consul.d/"
        ]
        network_mode = "host"
      }

      template {
        destination     = "local/haproxy.cfg"
        env             = false
        change_mode = "restart"
        left_delimiter  = "{{"
        right_delimiter = "}}"
        data            = <<EOH
defaults
  mode http
  retries 3
  option  redispatch
  timeout client 30s
  timeout connect 5s
  timeout server 30s

frontend stats
  bind *:1936
  stats uri /
  stats show-legends
  no log

frontend http_front
  bind *:{{ env "NOMAD_PORT_http" }}
  bind *:{{ env "NOMAD_PORT_https" }} ssl crt /etc/ssl/certs/_.service.consul.pem
  http-request set-header X-Forwarded-Proto https if { ssl_fc }
  http-request redirect scheme https unless { ssl_fc }
{{ range $tag, $services := services | byTag }}{{ if eq $tag "proxy" }}{{ range $service := $services }}{{ if ne .Name "haproxy" }}
  acl host_{{ .Name }} hdr(host) -i {{ .Name }}.service.consul
  use_backend {{ .Name }} if host_{{ .Name }}
{{ end }}{{ end }}{{ end }}{{ end }}
{{ range $tag, $services := services | byTag }}{{ if eq $tag "proxy" }}{{ range $service := $services }}{{ if ne .Name "haproxy" }}
backend {{ .Name }}
  balance roundrobin
  server-template {{ .Name }} 10 _{{ .Name }}._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check
{{ end }}{{ end }}{{ end }}{{ end }}
resolvers consul
  nameserver consul 127.0.0.1:8600
  accepted_payload_size 8192
  hold valid 5s

EOH
      }
    }
  }
}
