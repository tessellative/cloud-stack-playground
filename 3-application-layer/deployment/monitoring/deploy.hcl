job "monitoring" {

  type        = "service"
  datacenters = ["[[ .datacenter ]]"]

  group "prometheus_grp" {
    network {
      port "prom_port" {
        static = 9090
      }
    }

    ephemeral_disk {
      migrate = true
      sticky  = true
    }

    task "prometheus" {
      driver = "podman"

      config {
        image = "docker.io/prom/prometheus:latest"
        ports = ["prom_port"]

        network_mode = "host"

        volumes = [
          "/etc/ssl/certs/ca-bundle.crt:/etc/ssl/certs/ca-certificates.crt:ro",
          "local:/prometheus"
        ]

        args = [
          "--config.file=/prometheus/prometheus.yml",
          "--storage.tsdb.path=/prometheus/data"
        ]

        labels {
          group = "monitoring"
        }

      }

      service {
        address_mode = "host"
        tags = ["monitoring", "proxy"]
        name = "prometheus"
        port = "prom_port"
        tags = [
#          "traefik.enable=true",
#          "traefik.http.middlewares.httpsRedirect.redirectscheme.scheme=https",
#          "traefik.http.middlewares.httpsRedirect.redirectscheme.permanent=true",
#          "traefik.http.routers.${NOMAD_TASK_NAME}_insecure.middlewares=httpsRedirect",
#          "traefik.http.routers.${NOMAD_TASK_NAME}.tls.domains[0].main=${NOMAD_TASK_NAME}.[[ .tld ]]"
        ]
        check {
          type     = "tcp"
          port     = "prom_port"
          interval = "15s"
          timeout  = "2s"
        }
      }
      template {
        destination   = "local/prometheus.yml"
        change_mode   = "signal"
        change_signal = "SIGHUP"
        data          = <<EOH
global:
  scrape_interval: 15s # By default, scrape targets every 15 seconds.
  evaluation_interval: 15s # By default, scrape targets every 15 seconds.

  external_labels:
    monitor: 'cloud-playground'

scrape_configs:
  - job_name: 'static-hosts'
    static_configs:
      - targets:
          - "node01.node.cloud-playground.consul:9100"
          - "node02.node.cloud-playground.consul:9100"
          - "node03.node.cloud-playground.consul:9100"

  - job_name: 'nomad-client'
    consul_sd_configs:
      - server: 'https://consul.service.consul:8501'
        services: [ 'nomad-client' ]
        tags: [ 'http' ]
        scheme: http
    scrape_interval: 10s
    metrics_path: /v1/metrics
    params:
      format: [ 'prometheus' ]
    relabel_configs:
      - source_labels: [ '__meta_consul_dc' ]
        target_label: 'dc'
      - source_labels: [ __meta_consul_service ]
        target_label: 'job'
      - source_labels: [ '__meta_consul_node' ]
        target_label: 'host'
EOH
      }
    }
  }

  group "grafana_grp" {
    network {
      port "grafana_port" {
        static = 3000
      }
    }

    ephemeral_disk {
      migrate = true
      sticky  = true
    }

    task "grafana" {
      driver = "podman"

      config {
        image = "docker.io/grafana/grafana:latest"

        network_mode = "host"

        volumes = [
          "/etc/ssl/certs/ca-bundle.crt:/etc/ssl/certs/ca-certificates.crt:ro",
          "local:/var/lib/grafana"
        ]

        labels {
          group = "monitoring"
        }

      }

      env {
        GF_AUTH_ANONYMOUS_ENABLED  = "true"
        GF_SECURITY_ADMIN_PASSWORD = "admin"
        GF_USERS_ALLOW_SIGN_UP     = "false"
      }

      service {
        tags = ["monitoring", "proxy"]
        name = "grafana"
        port = "grafana_port"
        address_mode = "host"
        tags = [
#          "traefik.enable=true",
#          "traefik.http.middlewares.httpsRedirect.redirectscheme.scheme=https",
#          "traefik.http.middlewares.httpsRedirect.redirectscheme.permanent=true",
#          "traefik.http.routers.${NOMAD_TASK_NAME}_insecure.middlewares=httpsRedirect",
#          "traefik.http.routers.${NOMAD_TASK_NAME}.tls.domains[0].main=${NOMAD_TASK_NAME}.[[ .tld ]]"
        ]

        check {
          type     = "tcp"
          port     = "grafana_port"
          interval = "15s"
          timeout  = "2s"
        }
      }
    }
  }
}
